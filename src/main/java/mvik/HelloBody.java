package mvik;

import io.micronaut.core.annotation.Introspected;
import lombok.Data;

@Data
@Introspected
public class HelloBody {

    private String message;
}
