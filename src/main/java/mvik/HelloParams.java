package mvik;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.annotation.QueryValue;
import lombok.Data;

// tag::request-bean[]
@Data
@Introspected
public class HelloParams {

    @QueryValue private String name;
    @QueryValue private String hobby;
}
// end::request-bean[]
