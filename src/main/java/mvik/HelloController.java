package mvik;

import io.micronaut.http.annotation.*;

import java.util.Collections;
import java.util.Map;

// tag::hello-controller-class-def[]
@Controller
public class HelloController {
// end::hello-controller-class-def[]

    /**
     * This method uses a request bean pojo.
     *
     * The generated OpenApi spec uses a schema ref, I would have liked it to match {@link this#getUsingQueryValues}.
     */
    // tag::hello-controller-method[]
    @Get("/hello/request-bean-params")
    public Map<?, ?> getUsingRequestBean(@RequestBean HelloParams params) {
        return Collections.singletonMap("hello", params.getName());
    }
    // end::hello-controller-method[]

    /**
     * This method uses query values.
     *
     * The generated OpenApi spec looks good.
     */
    @Get("/hello/query-params")
    public Map<?, ?> getUsingQueryValues(@QueryValue String name, @QueryValue String hobby) {
        return Collections.singletonMap("hello", name);
    }

    @Post("/hello/request-bean-params")
    public Map<?, ?> postUsingRequestBeanAndBody(@RequestBean HelloParams params, @Body HelloBody body) {
        return Collections.singletonMap(params.getName(), body.getMessage());
    }

    @Post("/hello/query-params")
    public Map<?, ?> postUsingQueryValuesAndBody(@QueryValue String name, @QueryValue String hobby, @Body HelloBody body) {
        return Collections.singletonMap("hello", name);
    }
}
