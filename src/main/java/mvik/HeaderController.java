package mvik;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.RequestBean;

import java.util.Collections;
import java.util.Map;

import static mvik.HeaderParams.X_CUSTOM_HEADER;

// tag::header-controller-class-def[]
@Controller
public class HeaderController {
// end::header-controller-class-def[]

    // tag::header-controller-method[]
    @Get("/headers:request-bean-params")
    public Map<?, ?> getUsingHeaderRequestBean(@RequestBean HeaderParams headers) {
        return Collections.singletonMap("headerValue", headers.getCustomHeader());
    }
    // end::header-controller-method[]

    @Get("/headers:header-params")
    public Map<?, ?> getUsingHeaderParam(@Header(X_CUSTOM_HEADER) String customHeader) {
        return Collections.singletonMap("headerValue", customHeader);
    }
}
