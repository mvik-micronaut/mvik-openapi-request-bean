package mvik;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.annotation.Header;
import lombok.Data;

@Data
@Introspected
public class HeaderParams {

    public static final String X_CUSTOM_HEADER = "X-CUSTOM-HEADER";

    @Header(X_CUSTOM_HEADER)
    private String customHeader;
}
